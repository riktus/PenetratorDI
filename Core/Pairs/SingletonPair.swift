//
//  SingletonPair.swift
//  PenetratorDI
//
//  Created by Aleksandr Volkov on 16/01/2018.
//

import Foundation

class SingletonPair {
    var baseType: Any.Type!
    var object: NSObject!
}
