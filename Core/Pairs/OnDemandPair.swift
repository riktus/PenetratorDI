//
//  OnDemandPair.swift
//  PenetratorDI
//
//  Created by Aleksandr Volkov on 16/01/2018.
//

import Foundation

class OnDemandPair {
    var baseType: Any.Type?
    var castType: NSObject.Type?
    var lifecycle: LifecycleType!
}
