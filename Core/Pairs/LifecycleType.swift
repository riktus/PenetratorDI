//
//  LifecycleType.swift
//  PenetratorDI
//
//  Created by Aleksandr Volkov on 16/01/2018.
//

import Foundation

public enum LifecycleType {
    case onDemand
    case asSingleton
}
