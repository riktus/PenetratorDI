//
//  Penetrator.swift
//  PenetratorDI
//
//  Created by Aleksandr Volkov on 16/01/2018.
//

import Foundation

public class Container {
    private static var mRegisterSingletons: [SingletonPair] = []
    
    private(set) static var registerTypes: [OnDemandPair] = []
    private(set) static var namedRegisterTypes: [String: OnDemandPair] = [:]
    
    /**
     Регистрация типов
     * Base - протокол, должен быть помечен атрибутом @objc
     * Cast - класс, должен быть наследником NSObject и помечен атрибутом @objcMembers
     */
    public static func registerType<Base: Any, Cast: NSObject>(baseType: Base.Type, castType: Cast.Type, _ lifetime: LifecycleType = .onDemand, name: String? = nil) {
        if lifetime == .onDemand {
            let pair = OnDemandPair()
            pair.baseType = baseType
            pair.castType = castType
            pair.lifecycle = lifetime
            
            if name == nil {
                registerTypes.append(pair)
            } else {
                namedRegisterTypes[name!] = pair
            }
        } else {
            
        }
    }
    
    public static func removeType<T: Any>(proto: T.Type) {
        if let index = registerTypes.index(where: { $0.baseType == proto }) {
            registerTypes.remove(at: index)
        }
    }
    
    /**
     Получение инстанса по протоколу
     */
    public static func get<Proto: Any>(ofType: Proto.Type, name: String? = nil) -> Proto? {
        var targetPair: OnDemandPair = OnDemandPair()
        
        if name == nil {
            if let pair = registerTypes.first(where: { $0.baseType! == ofType}) {
                targetPair = pair
            }
        } else {
            if let pair = namedRegisterTypes.first(where: { $0.key == name! }) {
                targetPair = pair.value
            }
        }
        
        switch targetPair.lifecycle {
        case .asSingleton:
            let object = getSingleton(type: ofType, castType: targetPair.castType!) as! Proto
            
            //fillPropertyInjections(object: object as! NSObject)
            
            return object
        case .onDemand:
            let type = targetPair.castType!
            let object = type.init()
            
            //fillPropertyInjections(object: object)
            
            return (object as! Proto)
        default:
            break
        }
        
        return nil
    }
    
    /**
     Получение синглтона
     */
    private static func getSingleton(type: Any.Type, castType: NSObject.Type) -> NSObject {
        if let pair = mRegisterSingletons.first(where: { $0.baseType == type }) {
            return pair.object
        } else {
            let object = castType.init()
            let singPair = SingletonPair()
            singPair.baseType = type
            singPair.object = object
            
            mRegisterSingletons.append(singPair)
            
            return object
        }
    }
    
    /**
     Внедрение зависимостей в свойства объекта
     */
//    private static func fillPropertyInjections(object: NSObject) {
//        let mirror = Mirror(reflecting: object)
//        let dependencyChildrens = mirror.children.filter({ $0.label != nil && $0.label!.hasPrefix("inject")})
//
//        dependencyChildrens.forEach({ child in
//            guard let pair = registerTypes.first(where: { $0.baseType == type(of: child.value) }) else { return }
//
//            let cast = pair.castType!
//            let injectable = cast.init()
//
//            fillPropertyInjections(object: injectable)
//
//            object.setValue(injectable, forKey: child.label!)
//        })
//    }
}
