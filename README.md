# PenetratorDI

[![CI Status](http://img.shields.io/travis/Aleksandr Volkov/PenetratorDI.svg?style=flat)](https://travis-ci.org/Aleksandr Volkov/PenetratorDI)
[![Version](https://img.shields.io/cocoapods/v/PenetratorDI.svg?style=flat)](http://cocoapods.org/pods/PenetratorDI)
[![License](https://img.shields.io/cocoapods/l/PenetratorDI.svg?style=flat)](http://cocoapods.org/pods/PenetratorDI)
[![Platform](https://img.shields.io/cocoapods/p/PenetratorDI.svg?style=flat)](http://cocoapods.org/pods/PenetratorDI)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PenetratorDI is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'PenetratorDI'
```

## Author

Aleksandr Volkov, riktus@protonmail.com

## License

PenetratorDI is available under the MIT license. See the LICENSE file for more info.
